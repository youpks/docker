# Youpks Docker

```yaml
version: '3.8'

services:

  php:
    image: registry.gitlab.com/youpks/docker:php-8.1
    working_dir: /var/www/html
    environment:
      PHP_SESSION_HANDLER: ${REDIS_HOST}
      PHP_SESSION_PATH: tcp://${REDIS_HOST}:${REDIS_PORT:-6379}
    volumes:
      - ./app:/var/www/html:rw
    networks:
      - internal
    logging:
      driver: "json-file"
      options:
        max-size: "200k"
        max-file: "10"

  database:
    image: registry.gitlab.com/youpks/docker:mysql-8.0
    environment:
      MYSQL_DATABASE: ${DB_DATABASE}
      MYSQL_USER: ${DB_USERNAME}
      MYSQL_PASSWORD: ${DB_PASSWORD}
      MYSQL_ROOT_PASSWORD: ${DB_ROOT_PASSWORD:-secret}
    networks:
      - internal
    ports:
      - ${FORWARD_DB_PORT:-3306}:3306

  nginx:
    image: registry.gitlab.com/youpks/docker:nginx-stable-alpine
    working_dir: /var/www/html
    volumes:
      - ./app:/var/www/html
    links:
      - php
    labels:
      traefik.backend: ${APP_DOMAIN}
      traefik.frontend.rule: Host:${APP_DOMAIN}
      traefik.docker.network: traefik
      traefik.port: 80
    networks:
      - internal
      - traefik

  redis:
    image: registry.gitlab.com/youpks/docker:redis-4-alpine
    networks:
      - internal

networks:
  traefik:
    external: true
  internal:
    external: false




```